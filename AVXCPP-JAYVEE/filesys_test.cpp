#include <iostream>
#include "helpers/Headers.h"
#include "helpers/FileManager.hpp"

//g++ filesys_test.cpp -std=c++11 -ID:\AVXCPP\include -LD:\AVXCPP\lib\ffmpeg -o filesys_test -lavformat.dll -lavutil.dll -lavcodec.dll

std::string getMetaData(std::string data);
std::string getDirectoryTree(std::string data);

int main()
{
	//char path[256] = "D:\\AVXCPP\\data\\3 Idiots 2009 1080p BluRay x264 Hindi AAC - Ozlem.mp4";
	/*const char *path = "D:\\AVXCPP";
	char dirtree[65000] = {'\0'};
	//std::cout << FileManager::traverseDirectory(dirtree, path);
	FileManager::traverseDirectory(dirtree, path);
	std::string tree = dirtree;
	std::cout << tree;*/
	
	std::string path = "D:\\AVXCPP-JAYVEE\\Open Shut Them   More - Super Simple Songs.mp4";
	std::cout << getMetaData(path) << std::endl;
	std::cout << " ---------------------------------------------- " << std::endl;
	std::cout << getDirectoryTree(path) << std::endl;
	
	
	return 0;
}

std::string getMetaData(std::string data)
{
	char pathFile[256] = {'0'};
	strcpy(pathFile, data.c_str()); // for metadata

	return FileManager::get_metadata(pathFile);
}

std::string getDirectoryTree(std::string data)
{
	//std::replace(data.begin(), data.end(), '\\', '/'); 

	char pathParent[256] = {'0'}; // for directory tree
	strcpy(pathParent, data.c_str());
	*(strrchr(pathParent, '\\') + 1) = 0;

	char dirtree[MAX_BUFFER] = {'\0'};
	FileManager::traverseDirectory(dirtree, pathParent);
	std::string tree = dirtree;
	
	return tree;
}
