#pragma once

#include <string>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <unordered_map>

class TimeManager
{
	public:
		TimeManager();
		~TimeManager();

		static std::string timetostr(long duration);
		static std::string timetostr();
		static long timestamp();
		
		void addInterval(std::string key, unsigned seconds);
		void removeInterval(std::string key);
		void updateIntervals(float dt);
		bool isIntervalTriggered(std::string key);
		float update();
		
	private:
		struct Interval
		{
			float progress;
			unsigned ceiling;
			bool flag;
		};
		
		float m_deltaTime;
		std::chrono::high_resolution_clock::time_point m_begin;
		std::chrono::high_resolution_clock::time_point m_end;
		std::unordered_map<std::string, Interval> m_intervals;
};

TimeManager::TimeManager()
{
	m_deltaTime = 0;
	m_begin = std::chrono::high_resolution_clock::now();
	m_end = m_begin;
}

std::string TimeManager::timetostr(long duration)
{
	std::time_t t = duration;
	
	char buffer [80];
	strftime (buffer, 80, "%Y-%m-%d %X", localtime(&t));
	
	return buffer;
}

std::string TimeManager::timetostr()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	
	std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
	
	return ss.str();
}

long TimeManager::timestamp()
{
	auto now = std::chrono::system_clock::now();
	auto now_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
	auto epoch = now_ms.time_since_epoch();
	auto value = std::chrono::duration_cast<std::chrono::seconds>(epoch);
	
	return value.count();
}

void TimeManager::addInterval(std::string key, unsigned seconds)
{
	Interval interval;
	interval.progress = 0;
	interval.ceiling = seconds;
	interval.flag = false;
	
	m_intervals[key] = interval;
}

void TimeManager::removeInterval(std::string key)
{
	if (m_intervals.count(key) > 0)
		m_intervals.erase(key);
}

void TimeManager::updateIntervals(float dt)
{
	for (auto& i : m_intervals)
	{
		if ((unsigned)i.second.progress < i.second.ceiling)
		{
			i.second.progress += dt;
			i.second.flag = false;
		}
		else
		{
			i.second.progress = 0;
			i.second.flag = true;
		}
	}
}

bool TimeManager::isIntervalTriggered(std::string key)
{
	if (m_intervals.count(key) > 0)
		return m_intervals[key].flag;
	
	return false;
}

float TimeManager::update()
{
	m_begin = std::chrono::high_resolution_clock::now();
	m_deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(m_begin - m_end).count();
	m_end = m_begin;
	
	return m_deltaTime;
}

TimeManager::~TimeManager()
{
	// placeholder
}
