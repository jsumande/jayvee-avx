#pragma once

#include <iostream>

class Cryptography
{
	public:
		static std::string encrypt(std::string message, std::string key)
		{
			std::string processedMessage(message);
			
			unsigned int iKey(key.length()), iIn(message.length()), x(0);
			
			for(unsigned int i = 0; i < iIn; i++)
			{
				processedMessage[i] = message[i] ^ key[x];
				
				if (++x == iKey)
				{ 
					x = 0; 
				}
			}
			
			return processedMessage;
		}
		
        static std::string decrypt(std::string message, std::string key)
		{
			std::string processedMessage(message);
			
			unsigned int iKey(key.length()), iIn(message.length()), x(0);
			
			for(unsigned int i = 0; i < iIn; i++)
			{
				processedMessage[i] = message[i] ^ key[x];
				
				if(++x == iKey)
				{ 
					x = 0; 
				}
			}
			
			return processedMessage;
		}
};
