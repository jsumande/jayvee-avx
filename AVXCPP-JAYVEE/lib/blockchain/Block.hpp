#pragma once

#include "../../helpers/headers.h"
#include "../../helpers/cryptic/picosha2.h"

class Block
{
	public:
		Block(int index, long timestamp, std::string data);
		~Block();
		
		std::string calculateHash();
		
		std::string getHash();
		void setHash(std::string h);
		
		std::string getPrevHash();
		void setPrevHash(std::string h);
		
		std::string getData();
		std::string getTimeStamp();

	private:
		int index;
		std::string prevHash;	
		std::string data;
		std::string hash;
		long timestamp;
};

Block::Block(int index, long timestamp, std::string data)
{

	this->index = index; 
	this->timestamp = timestamp; 
	this->data = data; 
	this->hash = this->calculateHash(); 
}

std::string Block::calculateHash()
{
	return picosha2::hash256_hex_string(
		std::to_string(this->index) + this->prevHash + std::to_string(this->timestamp) + this->data
	);
}

std::string Block::getHash()
{
	return this->hash;
}

void Block::setHash(std::string h)
{
	this->hash = h;
}

std::string Block::getPrevHash()
{
	return this->prevHash;
}

void Block::setPrevHash(std::string h)
{
	this->prevHash = h;
}

std::string Block::getData()
{
	return this->data;
}


Block::~Block()
{
	// placeholder
}
