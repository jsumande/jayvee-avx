#pragma once

#include <iostream>
#include <vector>

#include "Block.hpp"

#include "../../helpers/headers.h"
#include "../../helpers/TimeManager.hpp"
#include "../../helpers/FileManager.hpp"

class Blockchain
{
	public:
		Blockchain(std::string chainDirectory);
		~Blockchain();
		
		Block* createGenesisBlock();
		void addBlock(Block* block);
		Block* getLatestBlock();
		bool isChainValid();
		std::string getblockChain();
		
		std::vector<Block*> chain;
		
	private:
		int chainType;
};

Blockchain::Blockchain(std::string chainDirectory)
{
	ULL len = FileManager::getFileSize(chainDirectory);
	
	this->chain.push_back(this->createGenesisBlock());
	
	json blockChain = json::parse(FileManager::readFromFile(chainDirectory, 0, len));
	std::cout << "--------Reading Chain--------" << std::endl;

	for(int idx = 1; idx <= blockChain.size(); idx++)
	{	
		this->addBlock(
			new Block(
				idx, 
				blockChain[std::to_string(idx)]["timestamp"].get<ULL>(),
				blockChain[std::to_string(idx)]["transactions"].dump()
			)
		);
	}

}

Block* Blockchain::createGenesisBlock()
{
	return new Block(0, TimeManager::timestamp(), "AVX Genesis Block");
}

void Blockchain::addBlock(Block* block)
{
	block->setPrevHash(this->getLatestBlock()->getHash());
	block->setHash(block->calculateHash());
	
	this->chain.push_back(block);
}

Block* Blockchain::getLatestBlock()
{
	return this->chain.back();
}


bool Blockchain::isChainValid()
{
	for (int i = 1; i < this->chain.size(); i++)
	{ 
		Block* currentBlock = this->chain[i]; 
		Block* previousBlock = this->chain[i - 1]; 
		
		if (currentBlock->getHash() != currentBlock->calculateHash()) 
		{ 
			return false; 
		} 

		if (currentBlock->getPrevHash() != previousBlock->getHash()) 
		{ 
			return false; 
		}
	} 
	
	return true; 
}



Blockchain::~Blockchain()
{
	for (int i = this->chain.size() - 1; i >= 0 ; --i)
	{
		delete this->chain[i];
		
		this->chain.erase(this->chain.begin() + i);
	}
}
