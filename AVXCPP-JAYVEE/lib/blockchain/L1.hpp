#pragma once

#include <blockchain/Blockchain.hpp>

#include "../../helpers/cryptic/base64.hpp"
#include "../../helpers/headers.h"
#include "../../helpers/TimeManager.hpp"
#include "../../helpers/FileManager.hpp"

class L1
{
	public:
		L1();
		~L1();
		
		int validate(std::string jsonData);
		double getBalance(std::string publicKey);
		void addBlock(std::string data);
		
		std::string getAssets();
		std::string thumbnail(std::string data);
		std::string getSeeders(std::string data);
		std::string getMetaData(std::string data);
		std::string getDirectoryTree(std::string data);
	
	protected:
		Blockchain *currencyBlockchain;
		Blockchain *assetBlockchain;

	private:
		double unverifiedSpent = 0; 
};

L1::L1()
{
	this->currencyBlockchain = new Blockchain(CURRENCY_CHAIN_PATH);
	this->assetBlockchain = new Blockchain(ASSET_CHAIN_PATH);
}

int L1::validate(std::string jsonData)
{
	json data = json::parse(jsonData);
	
	std::string receiver = data["receiver"].get<std::string>();
	std::string sender = data["sender"].get<std::string>();
	double amount = data["amount"].get<double>();
	double currentBalance = this->getBalance(sender);
	int type = data["type"].get<int>();	

	if (currentBalance >= amount)
	{
		this->unverifiedSpent = this->unverifiedSpent + amount;
		return true;
	}
	
	return false;	
}

double L1::getBalance(std::string publicKey)
{
	double balance = 0;

	for (int c = 1; c < this->currencyBlockchain->chain.size(); c++)
	{
		json j = json::parse(this->currencyBlockchain->chain[c]->getData());
		
		for (int k = 0; k < j.size(); k++)
		{
			if (j[std::to_string(k)]["receiver"].get<std::string>() == publicKey)
			{
				balance += j[std::to_string(k)]["amount"].get<double>();
			}
			
			if (j[std::to_string(k)]["sender"].get<std::string>() == publicKey)
			{
				balance -= j[std::to_string(k)]["amount"].get<double>();
			}
		}		
	}

	return balance - this->unverifiedSpent;
}

void L1::addBlock(std::string data)
{
	std::cout <<"**********/UPDATING BLOCKCHAINS/************" << std::endl;

	json j;
	j = json::parse(data);

	if (!j["currency"].is_null())
	{
		std::cout << "---- Adding Block to currency chain ----" << std::endl;
		this->currencyBlockchain->addBlock(new Block(
			this->currencyBlockchain->chain.size(), 
			TimeManager::timestamp(), 
			j["currency"].dump()
		));
	}
	
	if (!j["asset"].is_null())
	{
		std::cout << "---- Adding Block to asset chain ----" << std::endl;
		this->assetBlockchain->addBlock(new Block(
			this->assetBlockchain->chain.size(), 
			TimeManager::timestamp(), 
			j["asset"].dump()
		));
	}

	// remove on release
	std::cout <<"**********/CURRENCY BLOCKCHAIN/************" << std::endl;

	for (int x = 0; x < this->currencyBlockchain->chain.size(); x++)
	{
		std::cout << "-------------------------" << std::endl;
		std::cout << this->currencyBlockchain->chain[x]->getData() << std::endl;
	}

	std::cout <<"**********/ASSET BLOCKCHAIN/************" << std::endl;
	
	for (int x = 0; x < this->assetBlockchain->chain.size(); x++)
	{
		std::cout << "------------------------" << std::endl;
		std::cout << this->assetBlockchain->chain[x]->getData() << std::endl;
	}
	////////////////////

	//insert update json file



	this->unverifiedSpent = 0;
}

std::string L1::getAssets()
{
	json asset;
	int idx = 0;

	for (int i = 1; i < this->assetBlockchain->chain.size(); i++)
	{
		json data = json::parse(this->assetBlockchain->chain[i]->getData());

		for (int j = 0; j < data.size(); j++)
		{
			if(data[std::to_string(j)]["type"].get<int>() == BLOCKCHAIN_TX_UPLOAD)
			{
				asset[std::to_string(idx++)] = data[std::to_string(j)]["assetInfo"];
			}
		}
	}
	
	return asset.dump();
}

std::string L1::getSeeders(std::string data)
{
	json seeders;
	int idx = 0;

	for (int i = 1; i < this->assetBlockchain->chain.size(); i++)
	{
		json data = json::parse(this->assetBlockchain->chain[i]->getData());

		for (int j = 0; j < data.size(); j++)
		{
			if(data[std::to_string(j)]["type"].get<int>() == BLOCKCHAIN_TX_DOWNLOAD)
			{
				seeders[std::to_string(idx++)] = data[std::to_string(j)]["seeder"].get<std::string>();
			}
		}
	}
	
	return seeders.dump();
}

std::string L1::thumbnail(std::string filename)
{
	std::replace(filename.begin(), filename.end(), '\\', '/');
	
	unsigned long long filesize = FileManager::getFileSize(filename);
	char *buffer = FileManager::readFromFile(filename, 0, filesize - 1);

	return base64_encode(reinterpret_cast<const unsigned char*>(buffer), filesize);
}

std::string L1::getMetaData(std::string data)
{
	//std::replace(data.begin(), data.end(), '\\', '/'); 

	char pathFile[256] = {'0'};
	strcpy(pathFile, data.c_str()); // for metadata

	return FileManager::get_metadata(pathFile);
}

std::string L1::getDirectoryTree(std::string data)
{
	//std::replace(data.begin(), data.end(), '\\', '/'); 

	char pathParent[256] = {'0'}; // for directory tree
	strcpy(pathParent, data.c_str());
	*(strrchr(pathParent, '\\') + 1) = 0;

	char dirtree[MAX_BUFFER] = {'\0'};
	FileManager::traverseDirectory(dirtree, pathParent);
	std::string tree = dirtree;
	
	return tree;
}

L1::~L1()
{
	// placeholder
}
