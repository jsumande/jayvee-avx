#pragma once

#include <vector>
#include <unordered_map>

#include <blockchain/L1.hpp>
#include "../../helpers/cryptic/picosha2.h"

class L2 : public L1
{
	public:
		void addBlock();
		void addBlock(std::string data);
		std::string getBlock();
		std::string processTxPool(std::string data);

	private:
		std::unordered_map<std::string, double> m_ledger;
		void createLedger();
		json verifiedTransactions;
};

void L2::addBlock()
{
	L1::addBlock(this->verifiedTransactions.dump());
}

void L2::addBlock(std::string data)
{
	L1::addBlock(data);
}

std::string L2::getBlock()
{
	return this->verifiedTransactions.dump();
}

std::string L2::processTxPool(std::string data)
{
	json jdata = json::parse(data);
	int verifiedTransactionsCount = 0;
	
	this->verifiedTransactions.clear();	
	this->m_ledger.clear();
	this->createLedger();

	for (int i = 0; i < jdata.size(); i++)
	{	
		std::cout << "********************************" << std::endl;
		std::string sender = jdata[std::to_string(i)]["sender"].get<std::string>();
		std::cout << "********************************" << std::endl;
		std::string receiver = jdata[std::to_string(i)]["receiver"].get<std::string>();
		std::cout << "********************************" << std::endl;
		std::string paymentHash = jdata[std::to_string(i)]["paymentHash"].get<std::string>();
		std::cout << "********************************" << std::endl;
		double amount = jdata[std::to_string(i)]["amount"].get<double>();
		std::cout << "********************************" << std::endl;
		int type = jdata[std::to_string(i)]["type"].get<int>();
		std::cout << "********************************" << std::endl;

		if (this->m_ledger[sender] >= amount)
		{
			this->m_ledger[sender] -= amount;
			this->m_ledger[receiver]+= amount;

			json currency;

			currency["amount"] = amount;
			currency["sender"] = sender;
			currency["receiver"] = receiver;
			currency["type"] = type;
			currency["paymentHash"] = paymentHash;
			this->verifiedTransactions["currency"][std::to_string(verifiedTransactionsCount)] = currency; // merkle root

			if (type == BLOCKCHAIN_TX_UPLOAD)
			{
				json asset;
				std::cout << "********************************" << std::endl;
				asset["type"] = type;
				std::cout << "********************************" << std::endl;
				asset["price"] = jdata[std::to_string(i)]["price"].get<double>();
				std::cout << "********************************" << std::endl;
				asset["IPO"] = sender;
				std::cout << "********************************" << std::endl;
				asset["assetInfo"] = jdata[std::to_string(i)]["assetInfo"];
				std::cout << "********************************" << std::endl;
				asset["metadata"] = jdata[std::to_string(i)]["metadata"];
				std::cout << "********************************" << std::endl;
				asset["paymentHash"] = paymentHash;
				std::cout << "********************************" << std::endl;
				this->verifiedTransactions["asset"][std::to_string(verifiedTransactionsCount)] = asset;
			}

			/*
			if (type == BLOCKCHAIN_TX_DOWNLOAD)
			{
				json asset;
				asset["type"] = type;
				asset["price"] = jdata[std::to_string(i)]["price"].get<double>();
				asset["seeder"] = sender;
				asset["paymentHash"] = paymentHash;
				asset["trackingHash"] = jdata[std::to_string(i)]["trackingHash"].get<std::string>();
				this->verifiedTransactions["asset"][std::to_string(verifiedTransactionsCount)] = asset;
			}
			*/
			
			verifiedTransactionsCount++;
		}
	}

	return picosha2::hash256_hex_string(
		this->verifiedTransactions.dump()
	);	
}



void L2::createLedger()
{
	for (int c = 1; c < currencyBlockchain->chain.size(); c++)
	{
		json j = json::parse(currencyBlockchain->chain[c]->getData());
		for (int k = 0; k < j.size(); k++)
		{	
			std::string sender = j[std::to_string(k)]["sender"].get<std::string>();
			std::string receiver = j[std::to_string(k)]["receiver"].get<std::string>();
			double amount = j[std::to_string(k)]["amount"].get<double>();
			this->m_ledger[sender]-= amount;
			this->m_ledger[receiver]+= amount;
		}
	}

	/*
	for (auto const& t : Ledger)
	{
		std::cout << t.first << " = " << t.second << std::endl;


	}
	*/
}
