#pragma once

#include <typeinfo>

#include "../../helpers/Headers.h"
#include "../../helpers/TimeManager.hpp"
#include "../../helpers/FileManager.hpp"

#include "../../helpers/cryptic/base64.hpp"
#include "../../helpers/cryptic/Cryptography.hpp"
#include "../../helpers/cryptic/picosha2.h"

class Account
{
	public:
		Account();
		~Account();
		
		bool loginAccount(std::string username, std::string password);
		std::string registerAccount(std::string username, std::string password);
		std::string getPublicKey();
		
	private:
		std::string m_username;
		std::string m_password;
		std::string m_walletPath;
		std::string m_privateKey;
		std::string m_publicKey;
		
		std::string createPrivateKey(std::string ts);	
		std::string createPublicKey(std::string ts, std::string privateKey);	
		std::string getWallet();
		std::string getPrivKey();
		void createWallet();
		
};

Account::Account()
{
	// placeholder
}

bool Account::loginAccount(std::string username, std::string password)
{	
	m_username = username;
	m_password = password;

	std::string key = username + password + getPrivKey();
	std::cout << getPrivKey() << std::endl;
	std::string wallet = Cryptography::decrypt(getWallet(), key);
	
	std::cout << wallet << std::endl;
	if (wallet.find("decrypted") != std::string::npos)
	{
    	json j = json::parse(wallet);
		m_publicKey = j["publicKey"];

		return true;
	}
	
	return false;
}

std::string Account::registerAccount(std::string username, std::string password)
{
	m_username = username;
	m_password = password;

	this->createWallet();

	return m_privateKey;
}

std::string Account::createPublicKey(std::string ts, std::string privateKey)
{
	return KEY_PREFIX + picosha2::hash256_hex_string(
		m_username + m_password + ts + privateKey
	);	
}

std::string Account::createPrivateKey(std::string ts)
{			
	return KEY_PREFIX + picosha2::hash256_hex_string(
		m_username + m_password + ts
	);		
}

std::string Account::getPublicKey()
{
	return m_publicKey;
}

void Account::createWallet()
{
	std::string ts = std::to_string(TimeManager::timestamp());
	m_privateKey = createPrivateKey(ts);
	
	json data;
	data["privateKey"] = m_privateKey;
	data["publicKey"] = createPublicKey(ts, m_privateKey);
	data["balance"] = 0;
	data["username"] = m_username;
	data["password"] = m_password;
	data["status"] = "decrypted";

	std::string key = m_username + m_password + m_privateKey;	
	std::string encData = Cryptography::encrypt(data.dump(), key);

	FileManager::createBlankFile("D:/wallet/wallet.txt", encData.length());
	FileManager::writeToFile("D:/wallet/wallet.txt", &encData[0u], 0, encData.length());
	
	FileManager::createBlankFile("D:/wallet/pk.txt", m_privateKey.length());
	FileManager::writeToFile("D:/wallet/pk.txt", &m_privateKey[0u], 0, m_privateKey.length());
}

std::string Account::getWallet()
{
	ULL len = FileManager::getFileSize("D:/wallet/wallet.txt");
	
	return base64_decode(
		base64_encode(reinterpret_cast<const unsigned char*>(FileManager::readFromFile("D:/wallet/wallet.txt", 0, len)), len)
	);
}

std::string Account::getPrivKey()
{
	ULL len = FileManager::getFileSize("D:/wallet/pk.txt");
	
	return FileManager::readFromFile("D:/wallet/pk.txt", 0, len);
}


Account::~Account()
{
	// placeholder
}
