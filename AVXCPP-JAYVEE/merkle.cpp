#include <iostream>
#include <vector>
#include "lib/blockchain/picosha2.h"
#include "lib/blockchain/json.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <bits/stdc++.h> 
using json = nlohmann::json;
std::string hash(std::string a, std::string b);
std::string getMerkleRoot(std::vector< std::string > arr);
std::string decode(std::string a);
std::string swap(std::string a);


std::string decode(std::string a)
{
	int len = a.length();
	
	std::string newString;
	
	for(int i = 0; i < len; i += 2)
	{
		std::string byte = a.substr(i, 2);
		char chr = (int)strtol(byte.c_str(), nullptr, 16);
		newString.push_back(chr);
	}

	return newString;
}

std::string swap(std::string a)
{
	std::string b = "";
	
	for(int i = 0; i < a.length(); i+=2)
	{
		b += a.substr(i+1,1) + a.substr(i,1);
	}

	return b;
}

std::string hash(std::string a, std::string b)
{
	std::string decodedA = decode(a);
	std::reverse(decodedA.begin(), decodedA.end()); 
	std::string decodedB = decode(b);
	std::reverse(decodedB.begin(), decodedB.end()); 

	std::string hashed = picosha2::hash256_hex_string(
		decodedA + decodedB
	);	

	std::reverse(hashed.begin(), hashed.end()); 

	return swap(hashed);
}



std::string getMerkleRoot(std::vector<std::string> arr)
{
	std::vector<std::string> newArr;

	if (arr.size() == 1)
	{
		return arr[0];
	}

	for (int x = 0 ; x < arr.size() - 1 ; x += 2)
	{
		newArr.push_back(hash(arr[x], arr[x + 1]));
	}

	if (arr.size() & 1)
	{
		newArr.push_back(hash( arr[arr.size()-1],  arr[arr.size()-1]));
	}

	return getMerkleRoot(newArr);
	
}


int main()
{	

	std::vector<std::string> arr;
	std::ifstream file("txhash.in");
    std::string str;
	
    while (std::getline(file, str))
    {
		arr.push_back(str);
		
    }

	std::cout << "Merkle Root: " << getMerkleRoot(arr);
}