#include <stdio.h>
#include <iostream>
#include <vector>
#include <thread>
#include <unordered_map>
#include <typeinfo>

#include <network/P2PNetwork.hpp>
#include <blockchain/Account.hpp>
#include <blockchain/L1.hpp>
#include <blockchain/L2.hpp>

#include "helpers/Headers.h"
#include "helpers/Helper.hpp"
#include "helpers/FileManager.hpp"
#include "helpers/serializer/json.hpp"

P2PNetwork *p2p;

UINT port = 5150;
std::string trackerIP = "192.168.254.33";
UINT trackerPort = 7070;

std::string connectionInfo[2];
std::string disconnectionInfo[2];
std::string messageInfo[2];

std::string privateIP;
std::string msgBuffer;

void connectToTrackerServer();
void downloadComplete();

std::unordered_map<std::string, std::string> pubkeyIPList;

int main(int argc, char** argv)
{
	Account account;
	//L1 l1;
	L2 *Layer2 = new L2();	

	msgBuffer = "";

	if (argc > 1) port = std::stoi(argv[1]);

	p2p = new P2PNetwork(port);

	if(p2p->start())
	{
		std::cout << "P2P Network is running...........\n";		

		privateIP = p2p->getPrivateIP();
		//connectToTrackerServer();

		while(true)
		{
			if(p2p->getLocalServer()->hasActivity())
			{
				if(p2p->getLocalServer()->acceptNewConnection(connectionInfo))
				{
					std::cout << "New Connection IP: " << connectionInfo[0];
					std::cout << ", Port: " << connectionInfo[1] << std::endl;
				}

				// receive message from client
				if(p2p->getLocalServer()->onMessage(messageInfo))
				{
					//std::cout << "Message from client IP: " << messageInfo[0] << std::endl;
					std::cout << "*******************************************************" << std::endl;
					std::cout << ", Message: " << messageInfo[1] << std::endl;
					std::cout << "*******************************************************" << std::endl;
					msgBuffer += messageInfo[1];
					std::string senderIP = messageInfo[0];

					std::string tmpStr = Helper::parseNestedJSON(msgBuffer, true);

					if (tmpStr != "")
					{
						json msg = json::parse(tmpStr);
						int status = msg["status"].get<int>();
						if (status == 1)
						{
							// do something
							std::cout << msg.dump() << std::endl;
						}
						
						// JAYVEE ASSETS_UPLOAD

						else if (status == ASSETS_UPLOAD) // 9000
						{
							//json response;
							//std::string path = msg["data"]["filenamePath"].get<std::string>();
							//std::cout << "PATH: " << path << std::endl;
							std::string path = msg["data"]["filenamePath"].get<std::string>();
							char pathFile[256] = {'0'};
							strcpy(pathFile, path.c_str());
							std::string metadata = FileManager::get_metadata(pathFile);
							
							//const char *path = "D:\\AVXCPP-JAYVEE\\data\\Open Shut Them   More - Super Simple Songs.mp4";
							path = path.substr(0, path.find_last_of("\\/"));
							
							std::cout << "PATH: " << path << std::endl;
							
							/*response["file"] = msg["data"]["filenamePath"];
							response["status"] = ASSETS_UPLOAD;*/
							char tmpBuffer[MAX_BUFFER] = {'\0'};
							FileManager::traverseDirectory(tmpBuffer, path.c_str());
							std::string tree(tmpBuffer);
							
							std::string response = "{\"status\":" + std::to_string(ASSETS_UPLOAD) + ",";
							response += "\"data\":{\"tree\":" + tree + ",\"metadata\":" + metadata;
							response += "}}";
							
							std::cout << response << std::endl;
							
							p2p->sendToClient(messageInfo[0], &response[0u]);
							
						}
						
						else if (status == P2P_CONNECTION)
						{
							if(p2p->connect(port, messageInfo[0])) 
							{
								std::cout << "Connected to Port: " << port;
								std::cout << " IP: " << messageInfo[0] << std::endl;

								json a;
								a["status"] = ASSIGN_PUBKEY_IP;
								a["public_key"] = account.getPublicKey();

								p2p->sendMessage(messageInfo[0], &(a.dump())[0u]);
							}
							else
							{
								std::cout << "Cannot connect to Port: " << port;
								std::cout << " IP: " << messageInfo[0] << std::endl;
							}
						}

						else if (status == ASSIGN_PUBKEY_IP) // 1118
						{
							std::string pubkey = msg["public_key"].get<std::string>();
							pubkeyIPList[pubkey] = senderIP;
							
							for (auto const& ch : pubkeyIPList)
							{
								std::cout << ch.first << "->" << ch.second << std::endl;
							}
						}						
			
						else if (status == REGISTRATION) // 1113
						{
							std::string username = msg["data"]["username"].get<std::string>();
							std::string password = msg["data"]["password"].get<std::string>();

							json response;
							response["data"] = account.registerAccount(username,password);
							response["status"] = 1112;
							
							p2p->sendToClient(messageInfo[0], &(response.dump())[0u]);
						}

						else if (status == LOGIN) // 1114
						{
							std::string username = msg["data"]["username"].get<std::string>();
							std::string password = msg["data"]["password"].get<std::string>();
							std::cout << "here" << std::endl;

							json response;
							response["data"] = account.loginAccount(username, password);
							response["status"] = LOGIN;
							std::cout << response << std::endl;

							p2p->sendToClient(messageInfo[0], &(response.dump())[0u]);

							std::cout << "connecting to stun server after login " << std::endl;
							connectToTrackerServer();
						}

						//basic coin transfer
						else if (status == FUND_TRANSFER) // 1115
						{
							std::cout << "----Fund Transfer----" << std::endl;

							json response;
						
							response["response"] = Layer2->validate(msg["data"].dump());
							response["status"] = FUND_TRANSFER;
							
							p2p->sendToClient("127.0.0.1", &(response.dump())[0u]);
							if(response["response"] == 1)
							{
								msg["data"]["paymentHash"] = picosha2::hash256_hex_string(
									"paymentHash" + msg.dump() + std::to_string(TimeManager::timestamp())
								);	
								p2p->sendMessage("192.168.254.25", &(msg.dump())[0u]);
							}								
						}

						else if (status == FUND_TRANSFER_UPLOAD) // 1116
						{
							std::cout << "----Fund Transfer Upload----" << std::endl;

							msg["data"]["sender"] = account.getPublicKey();	
							msg["data"]["receiver"] = "avxhashhere";

							json response;
						
							response["response"] = Layer2->validate(msg["data"].dump());
							response["status"] = FUND_TRANSFER_UPLOAD;
							
							if (response["response"] == 1)
							{
								std::cout <<"validated" << std::endl;
								msg["data"]["assetInfo"]["trackingHash"] = picosha2::hash256_hex_string(
									"trackingHash" + msg.dump() + std::to_string(TimeManager::timestamp())
								);	

								msg["data"]["paymentHash"] = picosha2::hash256_hex_string(
									"paymentHash" + msg.dump() + std::to_string(TimeManager::timestamp())
								);	

								msg["data"]["metadata"] = json::parse(Layer2->getMetaData(msg["data"]["paths"]["filePath"].get<std::string>()));
								//msg["data"]["assetInfo"]["thumbnail"] = Layer2->thumbnail(msg["data"]["paths"]["thumbnailPath"].get<std::string>());
									
								response["directoryTree"] = Layer2->getDirectoryTree(msg["data"]["paths"]["filePath"].get<std::string>());

								msg["data"].erase("paths");

								p2p->sendToClient("127.0.0.1", &(response.dump())[0u]);
								p2p->sendMessage("192.168.254.25", &(msg.dump())[0u]);
							}		

							else 
							{
								p2p->sendToClient("127.0.0.1", &(response.dump())[0u]);
							}					
						}

						else if (status == FUND_TRANSFER_DOWNLOAD) // 1117
						{
							json response;
						
							response["response"] = Layer2->validate(msg["data"].dump());
							response["status"] = FUND_TRANSFER_DOWNLOAD;
							if(response["response"] == 1)
							{
								msg["data"]["paymentHash"] = picosha2::hash256_hex_string(
									"paymentHash" + msg.dump() + std::to_string(TimeManager::timestamp())
								);	

								p2p->sendMessage("192.168.254.25", &(msg.dump())[0u]);
							}							
						}

						else if (status == START_VALIDATION) // 1118
						{
							std::cout << "----validation is starting----" << std::endl;

							json response;

							response["data"]["hash"] = Layer2->processTxPool(msg["data"].dump());
							response["data"]["ip"] = privateIP;
							response["status"] = RECEIVE_CONSENSUS_HASHES;




							p2p->sendMessage("192.168.254.25", &(response.dump())[0u]);
						}

						else if (status == CREATE_BLOCK) // 1119
						{
							std::cout << "---- chosen to create block ----" << std::endl;

							Layer2->addBlock();

							std::cout <<"*** balance: " << Layer2->getBalance("1") << std::endl;
							std::cout <<"*** balance: " << Layer2->getBalance("0") << std::endl;
							json broadcast;  
							broadcast["status"] = RECEIVE_BLOCK;
							broadcast["data"] = Layer2->getBlock();

							std::string temp = Layer2->getAssets();

							std::cout << "////////////////////" << std::endl;
							std::cout << temp << std::endl;

							p2p->broadcast(&(broadcast.dump()[0u]));
						}

						else if (status == RECEIVE_BLOCK) // 1120
						{
							std::cout <<"--- block received ---" << std::endl;
							Layer2->addBlock(msg["data"]);
						}
						
						else if (status == GET_BALANCE) // 1121
						{
							json response;
							
							response["balance"] = Layer2->getBalance(account.getPublicKey());
							response["status"] = GET_BALANCE;
							p2p->sendToClient("127.0.0.1", &(response.dump())[0u]);
							
						}

						else if (status == GET_ASSETS) // 1122
						{
							json response;
							
							response["assets"] = Layer2->getAssets();
							response["status"] = GET_ASSETS;
							p2p->sendToClient("127.0.0.1", &(response.dump())[0u]);
							
						}

					}
				}

				if (p2p->getLocalServer()->hasNewDisconnection(disconnectionInfo))
				{
					std::cout << "New Disconnection IP: " << disconnectionInfo[0];
					std::cout << ", Port: " << disconnectionInfo[1] << std::endl;
					
					p2p->disconnect(disconnectionInfo[0]);
				}
			}
			
			//receive message from server
			p2p->recvFromServer([](char* str) {
				json msg = json::parse(std::string(str));
				int status = msg["status"].get<int>();
				
				if (status == 1)
				{
					p2p->getLocalServer()->stopClient(trackerIP);
					
					//std::cout << msg["rt"][0]["public_ip"].get<std::string>() << std::endl;
					for (auto const& m: msg["rt"])
					{
						if (m["type"].get<int>() == 1)
						{
							std::string ip = m["ip"].get<std::string>();

							if (privateIP != ip)
							{
								if(p2p->connect(port, ip)) 
								{
									std::cout << "Connected to Port: " << port;
									std::cout << " IP: " << ip << std::endl;
									
									json j;
									j["status"] = P2P_CONNECTION;
									
									p2p->sendMessage(ip, &(j.dump())[0u]);
								}
								else
								{
									std::cout << "Cannot connect to Port: " << port;
									std::cout << " IP: " << ip << std::endl;
								}
							}
						}
						else
						{
							// public
						}
					}
				}
			});
		}
	}
	else printf("P2P Network cannot run..\n");
	return 0;
}

void connectToTrackerServer()
{
	if(p2p->connect(trackerPort, trackerIP)) 
	{
		std::cout << "Connsadasdected to Port: " << trackerPort;
		std::cout << " IP: " << trackerIP << std::endl;
		
		json j;
		j["status"] = 1;
		j["privateIP"] = privateIP;
		
		p2p->sendMessage(trackerIP, &(j.dump())[0u]);
	}
	else
	{
		std::cout << "Cannot connect to Port: " << trackerPort;
		std::cout << " IP: " << trackerIP << std::endl;
	}
}

void downloadComplete(std::string seeders)
{
	json msg;
	msg["status"] = DOWNLOAD_COMPLETE;
	msg["data"] = seeders;
	p2p->sendMessage("192.168.254.25", &(msg.dump())[0u]);
}