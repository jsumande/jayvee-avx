#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <thread>
#include <unordered_map>
#include <map>
#include <typeinfo>
#include <time.h>

#include <network/P2PNetwork.hpp>

#include "helpers/Headers.h"
#include "helpers/Helper.hpp"
#include "helpers/TimeManager.hpp"
#include "helpers/serializer/json.hpp"

#define PENDING_THRESHOLD 5

struct IPDict
{
	UINT port;
	int limit;
	bool pending;
	
	IPDict(UINT p)
	{
		port = p;
		pending = false;
		limit = 0;
	}
};

P2PNetwork *p2p;

UINT serverPort = 5150;
UINT port = 5150;
std::string ip = "192.168.254.110";

std::string connectionInfo[2];
std::string disconnectionInfo[2];
std::string messageInfo[2];

std::string privateIP;
std::unordered_map<std::string, IPDict*> iplist;

void connect(std::string _ip, UINT _port, std::string _msg);

int main(int argc, char** argv)
{
	if (argc > 1) port = std::stoi(argv[1]);

	p2p = new P2PNetwork(port);
	
	/*std::ifstream t("mrhublot.hoard");
	std::string strHoard((std::istreambuf_iterator<char>(t)),
					 std::istreambuf_iterator<char>());
	
	json jsonHoard = json::parse(strHoard);
	std::cout << jsonHoard.dump() << std::endl;*/

	if(p2p->start())
	{
		std::cout << "P2P Network is running..\n";
		
		privateIP = p2p->getPrivateIP();
		std::cout << privateIP << std::endl;
		
		// connect to server
		if (privateIP != "192.168.254.105")
		{
			json j;
			j["status"] = 1;
			j["port"] = port;
			
			connect(ip, serverPort, j.dump());
		}

		TimeManager *tm = new TimeManager();

		while(true)
		{
			float dt = tm->update();
			tm->updateIntervals(dt);
			
			if (tm->isIntervalTriggered("reconnect"))
			{
				bool hasPending = false;
				
				for (auto it = iplist.begin(); it != iplist.end();) {
					if (it->second->limit >= PENDING_THRESHOLD) 
					{
						std::cout << "delete pending" << std::endl;
						it = iplist.erase(it);
					}
					else 
					{
						if (it->second->pending)
						{
							hasPending = true;
							std::cout << "recon" << std::endl;
							it->second->limit++;
						}
						it++;
					}
				}
				
				if (!hasPending)
				{
					std::cout << "stop recon" << std::endl;
					tm->removeInterval("reconnect");
				}
			}

			if(p2p->getLocalServer()->hasActivity())
			{
				std::cout << "vee" << std::endl;
				
				if(p2p->getLocalServer()->acceptNewConnection(connectionInfo))
				{
					std::cout << "New Connection IP: " << connectionInfo[0];
					std::cout << ", Port: " << connectionInfo[1] << std::endl;
				}

				// receive message from client
				if(p2p->getLocalServer()->onMessage(messageInfo))
				{
					std::cout << "Message from client IP: " << messageInfo[0];
					std::cout << ", Message: " << messageInfo[1] << std::endl;
					
					std::string senderIP = messageInfo[0];
					
					json msg = json::parse(messageInfo[1]);
					int status = msg["status"].get<int>();
					
					if (status == 1)
					{
						if (iplist.count(senderIP) > 0)
						{
							std::cout << "old ip recon" << std::endl;
							iplist[senderIP]->limit = 0;
							iplist[senderIP]->pending = false;
						}
						else
						{
							iplist[senderIP] = new IPDict(msg["port"].get<UINT>());
							
							// do something
							connect(senderIP, msg["port"].get<UINT>(), "");
						}
					}
			
				}

				if(p2p->getLocalServer()->hasNewDisconnection(disconnectionInfo))
				{
					std::cout << "New Disconnection IP: " << disconnectionInfo[0];
					std::cout << ", Port: " << disconnectionInfo[1] << std::endl;
					
					std::string nodeip = disconnectionInfo[0];
					
					p2p->disconnect(nodeip);
					
					tm->addInterval("reconnect", 5);
					iplist[nodeip]->pending = true;
				}
			}
			
			//receive message from server
			p2p->recvFromServer([](char* str) {
				
			});
		}
	}
	else printf("P2P Network cannot run..\n");
	
	for (auto& item : iplist)
	{
		delete item.second;
	}
	
	return 0;
}

void connect(std::string _ip, UINT _port, std::string _msg)
{
	if(p2p->connect(_port, _ip)) 
	{
		std::cout << "Connected to Port: " << _port;
		std::cout << " IP: " << _ip << std::endl;

		if (_msg != "")
			p2p->sendMessage(_ip, &(_msg)[0u]);
	}
	else
	{
		std::cout << "Cannot connect to Port: " << _port;
		std::cout << " IP: " << _ip << std::endl;
	}
}
