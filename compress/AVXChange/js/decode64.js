const base64toimage = require('base64-to-image');
const path = require('path');
const fs = require('fs');
var obj64 = fs.readFileSync('./json/base64img.json'); 
var json64 = JSON.parse(obj64);
var keys64 = Object.keys(json64);
var imgname = 'decodedimg';
var imgpath = path.join(__dirname,'/decodedimg/');
var data64 = "data:image/jpg;base64,";
//const $ = require('jquery');
//const ipcRender = electron.ipcRenderer;


function loadimg() {
	//document.getElementById('main1').src = data64 + json64[0];
	/*document.getElementById('main2').src = data64 + json64[1];
	document.getElementById('main3').src = data64 + json64[2];
	// document.getElementById('main4').src = data64 + json64[3];
	document.getElementById('main5').src = data64 + json64[4];
	// console.log(data64 + json64[0]);*/
	for (var i = 0; i < 7; ++i) {
		var movieThumbs = '<div class="col-xs-2">';
		movieThumbs += '<div>';
		movieThumbs += '<img src="' + data64 + json64[i]['thumbnail'] + '" />';
		movieThumbs += '<p id="filetitle">' + json64[i]['title'] + '</p>';
		movieThumbs += '<p id="movieyear">' + json64[i]['description'] + '</p>';
		movieThumbs += '</div>';
		movieThumbs += '</div>';
		
		$("#latest-movies").append(movieThumbs);
	}
	
	$("#latest-movies img").click(function() {
		//location.href = 'videoDetails.html';
		var jsonSeederList = {};
		for (var i in json64) {
			jsonSeederList[i] = {};
			jsonSeederList[i]["ipo"] = json64[i]["ipo"];
			jsonSeederList[i]["filename"] = json64[i]["filename"];
			jsonSeederList[i]["filesize"] = json64[i]["filesize"];
			jsonSeederList[i]["hash"] = json64[i]["hash"];
			jsonSeederList[i]["price"] = json64[i]["price"];
		}

		var jsonData = {
			seeders: jsonSeederList,
			details: json64[$(this).index()]
		};
		
		var jsonString = JSON.stringify(jsonData);
		ipcRender.send("show-video-details", jsonString);
	});
}