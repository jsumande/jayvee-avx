var msg = '';

$(document).ready(function() {
	
	ipcRender.send("request-transaction-history", '');
	
	ipcRenderer.on('transaction-history', function (event, data) {
		msg = JSON.parse(data);
		
		$("#Waddress").html(msg["wallet_address"]);
		$("#spentavx").html(parseFloat(msg["total_spent"]).toFixed(2));
		$("#earnedavx").html(parseFloat(msg["total_earned"]).toFixed(2));
		$("#totalBalance").html(parseFloat(msg["wallet_balance"]).toFixed(2));
		
		var histListID = ['merged-history', 'incoming-history', 'outgoing-history'];
		$('ul#list-history li a').click(function() {
			var idx = $(this).parent().index();
			
			var json = msg["history"];
			var status = ['pending', 'verified'];
			
			var html = '';
			for (var i in json) {
				if (idx == 0 || json[i].direction == idx - 1) {
					html += '<tr>';
						html += '<th scope="row"></th>';
						html += '<td>' + status[json[i].status] + '</td>';
						html += '<td>' + json[i].date + '</td>';
						html += '<td>' + json[i].time + '</td>';
						html += '<td>' + json[i].from + '</td>';
						html += '<td>' + json[i].to + '<td>';
						html += '<td>' + json[i].description + '</td>';
						html += '<td>' + json[i].amount + '</td>';
						html += '<td>' + json[i].transaction_id + '</td>';
					html += '</tr>';
				}
			}
			
			$('#' + histListID[idx] + ' tbody').html(html);
			
		});
	});
	
	
});
