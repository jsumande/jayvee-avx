$(document).ready(function() {	
	ipcRenderer.on('download-details', function (event, data) {
		console.log(data);
		var json = JSON.parse(data);
		
		$("#dlTable tbody tr td:eq(0)").html(json["filename"]);
		$("#dlTable tbody tr td:eq(2)").html(parseFloat(json["progress"]).toFixed(2) + '%');
	});
});