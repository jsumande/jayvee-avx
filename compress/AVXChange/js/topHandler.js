const $ = require('jquery');
const electron = require('electron');
const { remote } = require('electron');
const {ipcRenderer} = require('electron');
const ipcRender = electron.ipcRenderer;
var win = remote.getCurrentWindow();

// $('body').css('overflow', 'hidden');
$(document).ready(function() {
	$('#minimize').click(function() {
		win.minimize();
	});

	$('#maximize').click(function() {
		if(!win.isMaximized()) {
			win.maximize();
		} else {
			win.unmaximize();
		}
	});

	$('#close').click(function(){
		win.close();
	});
	
	$("#btnLogin").click(function() {
		var uname = $("#username").val();
		var pwd = $("#password").val();
		var filename = $('input[name="file"]').val().split('\\').pop();
		
		if (uname != "" && pwd != "" && filename != "") {
			var json = {
				status: 1002,
				data: {
					username: uname,
					password: pwd,
					import_wallet_location: filename
				}
			};
			
			var jsonString = JSON.stringify(json);
			
			ipcRender.send("login", jsonString);
		} else {
			alert("Invalid username / password / file!");
		}
		
		/*var json = {
			status: 1002,
			data: {
				username: "veanhart",
				password: "lugar",
				import_wallet_location: "data/12-12-2018-23-56-57.txt"
			}
		}
		
		var jsonString = JSON.stringify(json);
		
		ipcRender.send("login", jsonString);*/
	});
	
	$("#btnSignup").click(function() {
		/*var json = {
			status: 1001,
			data: {
				username: "veanhart",
				password: "lugar",
				export_wallet_location: "C:/"
			}
		}*/
		
		var uname = $("#username").val();
		var pwd = $("#password").val();
		
		if (uname != "" && pwd != "") {
			var json = {
				status: 1001,
				data: {
					username: uname,
					password: pwd,
					export_wallet_location: "C:/"
				}
			};

			var jsonString = JSON.stringify(json);
			
			ipcRender.send("signup", jsonString);
		} else {
			alert("Invalid username / password!");
		}
	});
	
	
	
});


