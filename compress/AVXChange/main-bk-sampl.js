var net = require('net');
var client = new net.Socket();

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const url = require('url');
const path = require('path');
const ipcMain = require('electron').ipcMain;
const bodyParser = require("body-parser");

var filename = ipcMain.addListener.toString;
var temp;
let defaultWindow; 
let notDefaultWindow;
let prevWindow;
var IMG_DIR = './images/';
var app_dir = './winPage/';
var tmpStr = '';

var walletBalance = 0;
var walletAddress = '';

// var filename;
function hidewindow() {
    BrowserWindow({
        show:false
    });
}

function showWindow() {
	// defaultWindow.openDevTools();
	defaultWindow = new BrowserWindow({
		frame: false,
		width: 1050,
		height: 750,
		minWidth: 850,
		minHeight: 600,
		backgroundColor: '#312450',
		icon: path.join(__dirname, IMG_DIR, 'whiteicon.png'),
		show: false,
	});

    defaultWindow.loadURL(url.format({
        pathname: path.join(__dirname, app_dir,'loginWindow.html'),
        //pathname: path.join(__dirname, app_dir,'accountWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

	defaultWindow.once('ready-to-show', () => {
		defaultWindow.show();
	});
	
	defaultWindow.on('close', () => {
		defaultWindow.show();
	});
}

function anotherWindow(filename) {
    notDefaultWindow = null;

    notDefaultWindow = new BrowserWindow({
		frame: false,
		maxHeight:612,
		maxWidth: 632,
		width: 631,
		height: 611,
		minWidth: 630,
		minHeight: 610,
		icon: path.join(__dirname, IMG_DIR, 'whiteicon.png'),
		parent: defaultWindow,
		modal: true
    });


	notDefaultWindow.loadURL(url.format({
		pathname: path.join(__dirname, app_dir, filename),
		protocol: 'file:',
		slashes: true
	}));
	
	notDefaultWindow.once('ready-to-show', () => {
		notDefaultWindow.show();
	});
}

let popupWindows;

function popupWindow(filename) {
    popupWindows = new BrowserWindow({
        frame: false,
        maxHeight:312,
        maxWidth: 352,
        width: 351,
        height: 311,
        minWidth: 310,
        minHeight: 350,
        icon: path.join(__dirname, IMG_DIR, 'whiteicon.png'),
        parent: defaultWindow,
        modal: true
    });
	
    popupWindows.loadURL(url.format({
        pathname: path.join(__dirname, app_dir, filename),
        protocol: 'file:',
        slashes: true
    }));

    popupWindows.once('ready-to-show', () => {
        popupWindows.show();
    });
}

ipcMain.on('popup', (event, arg) => {
    popupWindows(arg);
});

ipcMain.on('modal', (event, arg) => { 
    anotherWindow(arg);
});

app.on('quit', () => {
    console.log("closed");
});

app.on('ready',showWindow);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        app.quit();
    }
});


app.on('activate', () => {
    if(mainWindow == null) {
        showWindow();
    }
});

if(process.platform == 'darwin'){
    mainMenuTemplate.unshift({});
}

// add developer tools item if not in production

if(process.env.NODE_ENV == 'production')
{
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'Toggle Devtools',
                accelerator: process.platform == 'darwin' ? 'Command+I':
                'Ctrl+I',
                click(item,focusedWindow)
                {
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
            }
        ]
    });
}

var wb;
var avxtokens;
var logindatas;

ipcMain.on('test', () => {
    receiveee();
});

// ipcMain.on('wballance', (event,arg) => {

    // event.sender.send('wballance', wb.toString());

// });

ipcMain.on('sendingavx', (event, arg) => {
    event.sender.send('sendingavx', )
});

// ipcMain.on('sendAVX',(event, arg) => {
//     avxtokens = arg;
//     dataconnect(avxtokens);
// });

ipcMain.on('logindata',(event, arg) => {
    logindatas = arg;
    dataconnect(logindatas);
});


ipcMain.on('sendAVX',(event, arg) => {
    avxtokens = arg;
    dataconnect(avxtokens);
});

ipcMain.on('signup',(event, arg) => {
    //avxtokens = arg;
    client.write(arg);
});

ipcMain.on('login',(event, arg) => {
    //avxtokens = arg;
    client.write(arg);
});

ipcMain.on('show-video-details',(event, arg) => {
	/*let videoWindow = new BrowserWindow({
		frame: false,
		width: 1050,
		height: 750,
		minWidth: 850,
		minHeight: 600,
		backgroundColor: '#312450',
		icon: path.join(__dirname, IMG_DIR, 'whiteicon.png'),
		show: false,
	});*/

	//videoWindow.webContents.send('get-thumbnail', arg);
	//defaultWindow.webContents.send('get-thumbnail', arg);
	tmpStr = arg;
    defaultWindow.loadURL(url.format({
		//pathname: path.join(__dirname, app_dir,'loginWindow.html'),
		pathname: path.join(__dirname, app_dir, 'videoDetails.html'),
		protocol: 'file:',
		slashes: true
	}));

	
	/*defaultWindow.once('ready-to-show', () => {
        //videoWindow.show();
		defaultWindow.webContents.send('get-thumbnail', arg);
		defaultWindow.show();
    });*/
	
});

ipcMain.on('view-video-details',(event, arg) => {
	//console.log(tmpStr);
	defaultWindow.webContents.send('get-thumbnail', tmpStr);
});

ipcMain.on('request-transaction-history',(event, arg) => {
	//console.log(tmpStr);
	var json = { status: 1003 };
	client.write(JSON.stringify(json));
});

ipcMain.on('get-wallet-basic-info',(event, arg) => {
	//console.log(tmpStr);
	var json = { wbalance: walletBalance, waddress: walletAddress };
	console.log(json);
	notDefaultWindow.webContents.send('wallet-basic-info', JSON.stringify(json));
});

ipcMain.on('send-avx-transaction',(event, arg) => {
	client.write(arg);
});

ipcMain.on('send-download-request',(event, arg) => {
	defaultWindow.loadURL(url.format({
		//pathname: path.join(__dirname, app_dir,'loginWindow.html'),
		pathname: path.join(__dirname, app_dir, 'accountWindow3.html'),
		protocol: 'file:',
		slashes: true
	}));
	
	client.write(arg);
});

ipcMain.on('send-download-payment',(event, arg) => {
	defaultWindow.loadURL(url.format({
		//pathname: path.join(__dirname, app_dir,'loginWindow.html'),
		pathname: path.join(__dirname, app_dir, 'accountWindow3.html'),
		protocol: 'file:',
		slashes: true
	}));
	
	client.write(arg);
});


function dataconnect(logindatas) {
    //client.write(logindatas);
}

client.connect(5150, '192.168.254.33', function() {
//client.connect(5150, '127.0.0.1', function() {
	console.log('Connected');
	
	var j = {
		status: 1
	};
	client.write(JSON.stringify(j));
});

client.on('close', function() {
	console.log('Connection closed');
});

client.on('error', function(err) {
	console.log('Connection error ' + err);
});    

client.on('data', function(data) {
	var msg = JSON.parse(data);
	
	/*if (msg["status"] == 1001) {
		if (msg["data"]["valid"]) {
			console.log("valid");
			defaultWindow.loadURL(url.format({
				pathname: path.join(__dirname, app_dir, 'loginWindow.html'),
				protocol: 'file:',
				slashes: true
			}));
		} else {
			console.log("invalid");
		}
	} else if (msg["status"] == 1002) {
		if (msg["data"]["valid"]) {
			console.log("valid");
			defaultWindow.loadURL(url.format({
				pathname: path.join(__dirname, app_dir, 'mainWindow.html'),
				protocol: 'file:',
				slashes: true
			}));
		} else {
			console.log("invalid");
		}
	} else if (msg["status"] == 1003) {
		//console.log(msg);
		walletBalance = msg["data"]["wallet_balance"];
		walletAddress = msg["data"]["wallet_address"];
		
		defaultWindow.webContents.send('transaction-history', JSON.stringify(msg["data"]));
	} else if (msg["status"] == 4004) {
		defaultWindow.webContents.send('download-details', JSON.stringify(msg));
	}*/
	
	if (msg["status"] == 2) 
	{
		console.log(msg["data"]);
		
	}
	
	
   /* var data1 = JSON.parse(data);
    // console.log(data1);
    console.log(data1);

    if(data1["status"] == 1) {
        console.log("Status is 1");
        console.log("Access Denied");
    } else if(data1["status"] == 2) {
        console.log("Status is 2");
        defaultWindow.loadURL(url.format({
            pathname: path.join(__dirname, app_dir,'mainWindow.html'),
            protocol: 'file:',
            slashes: true
        }));
        // console.log(data1["public"]);
        ipcMain.on('waddress', (event,arg) => {
            event.sender.send('waddress',data1["public"]);
        });
    } else if(data1["status"] == 3) {
        console.log(data1["balance"]);
        
        wb = data1["balance"].toString();
        // console.log(wb);        // ipcMain.on('wballance', (event, arg) => { 
        //    event.Sender.send = data1["balance"];
        // });
    }
	
    if(data1["status"] == 777) {
        console.log(data1["historyjson"])
        console.log(data1);
    }*/
});
