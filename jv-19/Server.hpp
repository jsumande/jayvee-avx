#pragma once

#include <iostream>
#include <stdio.h>
#include <string>
#include <winsock2.h>
#include <unistd.h>
#include <sys/time.h>
#include <unordered_map>

typedef unsigned int UINT;

struct ClientInfo
{
    std::string ip;
    std::string port;
    ClientInfo(std::string ip, std::string port)
    {
        this->ip = ip;
        this->port = port;
    }
};

class Server
{
    private:
    UINT port;
    int masterSocket;
    //set of socket descriptors 
	fd_set readfds;
    struct timeval timeout;

    WSADATA wsaData;
    SOCKET ListeningSocket;
    SOCKADDR_IN serverAddr;
    SOCKADDR_IN clientAddr;
    int maxSD, sd, activity, newSocket, valread, addrlen, addlen;
    //int clients[10];
    std::unordered_map<int, ClientInfo*> clients;
    char buffer[65000 + 512] = { 0 };

    // SOCKET NewConnection;
    // SOCKADDR_IN ServerAddr;

    // Be careful with the array bound, provide some checking mechanism...
    char sendbuf[65000 + 512] = "";
    int BytesSent, nlen;

    ClientInfo *newDisconnectedClient;
    
    public:
    Server(UINT);
    bool init();
    void run(void (*)(std::string, std::string));
    bool createMasterSocket();
    void stopClient(std::string ip);
    void sendMessage(int sd, char sendBuff[]);
    void sendToClient(std::string ip, char sendBuff[]);

    bool hasActivity();
    bool acceptNewConnection(std::string *info);
    bool hasNewDisconnection(std::string *info);
    bool onMessage(std::string *info);
};

/*
    Start Server

    if(server->init()) 
	{
		if(server->createMasterSocket())
		{
			printf("Master Socket Created..\n");
			server->start();
		}
	}
*/

Server::Server(UINT port)
{
    this->port = port;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
}

void Server::sendMessage(int sd, char sendBuff[])
{
	std::cout << "sendbuff size: " << strlen(sendBuff) << std::endl;
    send(sd , sendBuff , strlen(sendBuff) , 0);
}

void Server::sendToClient(std::string ip, char sendBuff[])
{
	for(const auto& node : clients)
    {
        if (node.second->ip == ip) 
		{
			std::cout << "Sending to client ip: " << ip << std::endl;
			send(node.first, sendBuff, strlen(sendBuff), 0);
		}
	}
}

bool Server::createMasterSocket()
{
    //create a master socket 
    if( (masterSocket = socket(AF_INET , SOCK_STREAM , 0)) == 0)  
    {  
        printf("Master Socket Error: Failed\n");  
        return false;
    }  

    //type of socket created 
    serverAddr.sin_family = AF_INET;  
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    serverAddr.sin_port = htons(this->port);  
        
    //bind the socket to localhost port 8888 
    if (bind(masterSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)  
    {  
        printf("Master Socket Error: Bind failed, %s", WSAGetLastError());  
        return false;  
    }  
    
    //try to specify maximum of 3 pending connections for the master socket 
    if (listen(masterSocket, 5) < 0)  
    {  
        printf("Master Socket Error: Listen Failed\n");  
        return false;
    } 
	
	const int bufferSize = 65512;
    setsockopt(masterSocket, SOL_SOCKET, SO_SNDBUF, (char*)&bufferSize, sizeof(bufferSize));
    setsockopt(masterSocket, SOL_SOCKET, SO_RCVBUF, (char*)&bufferSize, sizeof(bufferSize));

    return true;
}

bool Server::init()
{
    // Initialize Winsock version 2.2
    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
    {
        /*
            MSDN: An application can call the WSAGetLastError() function to determine the
            extended error code for other Windows sockets functions as is normally
            done in Windows Sockets even if the WSAStartup function fails or the WSAStartup
            function was not called to properly initialize Windows Sockets before calling a
            Windows Sockets function. The WSAGetLastError() function is one of the only functions
            in the Winsock 2.2 DLL that can be called in the case of a WSAStartup failure
        */

        printf("Server Error: WSAStartup failed with error %ld\n", WSAGetLastError());
        // Exit with error
        return false;
    }
    // else
    // {
    //      printf("Server: The Winsock dll found!\n");
    //      printf("Server: The current status is %s.\n", wsaData.szSystemStatus);
    // }

    if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2 )
    {
        //Tell the user that we could not find a usable WinSock DLL
        printf("Server Error: The dll do not support the Winsock version %u.%u!\n", LOBYTE(wsaData.wVersion),HIBYTE(wsaData.wVersion));
        WSACleanup(); // Do the clean up
        return false; // and exit with error
    }
    // else
    // {
    //     printf("Server: The dll supports the Winsock version %u.%u!\n", LOBYTE(wsaData.wVersion), HIBYTE(wsaData.wVersion));
    //     printf("Server: The highest version this dll can support is %u.%u\n", LOBYTE(wsaData.wHighVersion), HIBYTE(wsaData.wHighVersion));
    // }

    return true;
}

bool Server::acceptNewConnection(std::string *info)
{
    //If something happened on the master socket, then its an incoming connection 

    if (FD_ISSET(masterSocket, &readfds))  
    {  
        addlen = sizeof(clientAddr);

        if ((newSocket = accept(masterSocket, (struct sockaddr *)&clientAddr, (int *)&addlen)) < 0)  
        { 
            printf("accept %s %d\n", WSAGetLastError(), masterSocket);  
            //exit(EXIT_FAILURE);  
            return false;
        }  
        
        //inform user of socket number - used in send and receive commands 
        // printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , 
        //     newSocket , 
        //     inet_ntoa(clientAddr.sin_addr) , 
        //     ntohs(serverAddr.sin_port));  
                
        std::string ip = inet_ntoa(clientAddr.sin_addr);
        std::string port = std::to_string(ntohs(serverAddr.sin_port));

        info[0] = ip;
        info[1] = port;

        ClientInfo *cInfo = new ClientInfo(inet_ntoa(clientAddr.sin_addr), port);
        clients[newSocket] = cInfo;
		
        return true;
    }   

    return false;
}

bool Server::onMessage(std::string *info)
{
    for(const auto& n : clients)
    {
        sd = n.first; //key

        if (FD_ISSET( sd , &readfds))  
        {  
            //Check if it was for closing , and also read the 
            //incoming message 
            valread = recv(sd , buffer, 65000, 0);

            if (valread == 0 || valread == SOCKET_ERROR)  //if someone disconnected
            {  
                //valread = 0            -> intentional close
                //valread = SOCKET_ERROR -> unintentional close

                //Somebody disconnected , get his details and print 
                getpeername(sd , (struct sockaddr*)&clients[sd]->ip, (int *)&addrlen);  
                // printf("Host disconnected , ip %s , port %d \n" , s
                //         inet_ntoa(serverAddr.sin_addr) , ntohs(serverAddr.sin_port));  

                //Close the socket and mark as 0 in list for reuse 
                newDisconnectedClient = new ClientInfo(clients[sd]->ip, std::to_string(this->port));

                close(sd);  
                clients.erase(sd);

                return false;
            }  
            else //Echo back the message that came in 
            {  
                //set the string terminating NULL byte on the end of the data read
				buffer[valread] = '\0';
                // printf("client %s: %s\n", clients[sd]->ip.c_str(), buffer);
                
                info[0] = clients[sd]->ip.c_str(); //IP
                info[1] = buffer; //Message
				//send(sd, buffer, strlen(buffer), 0);
				//std::cout << "test send" << std::endl;
				
                memset(buffer, 0, sizeof(buffer)); //Clearing Buffer
                return true;
            }  
        }
    }

    return false;
}

bool Server::hasActivity()
{
    //clear the socket set 
    FD_ZERO(&readfds);  

    //add master socket to set 
    FD_SET(masterSocket, &readfds);  
    maxSD = masterSocket;

    for(const auto& n : clients)
    {
        //socket descriptor 
        sd = n.first; //key
            
        //if valid socket descriptor then add to read list 
        if (sd > 0)  
            FD_SET( sd , &readfds);  
            
        //highest file descriptor number, need it for the select function 
        if (sd > maxSD)  
            maxSD = sd;  
    }

    //wait for an activity on one of the sockets , timeout is NULL , 
    //so wait indefinitely 
    
    activity = select(maxSD + 1 , &readfds , NULL , NULL , &timeout);
    
    if ((activity <= 0) && (errno!=EINTR))  
    {  
        //printf("select error");  
        return false;
    }
	//std::cout << activity << std::endl;
    return true;
}

bool Server::hasNewDisconnection(std::string *info)
{
    if(newDisconnectedClient != NULL)
    {
        info[0] = newDisconnectedClient->ip;
        info[1] = newDisconnectedClient->port;
        delete newDisconnectedClient;
        newDisconnectedClient = NULL;
        return true;
    }
    return false;
}

void Server::stopClient(std::string ip)
{
	for(const auto& n : clients)
    {
		if (n.second->ip == ip) {
			close(n.first);  
			clients.erase(n.first);
		}
	}
}
