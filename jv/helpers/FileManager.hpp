#pragma once

#include <fstream>
#include <dirent.h>
#include <cstring>

extern "C" {
	#include <libavformat/avformat.h>
	#include <libavformat/avio.h>
	#include <libavutil/file.h>
	#include <libavutil/channel_layout.h>
	#include <libavcodec/avcodec.h>
	#include <libavutil/pixdesc.h>
	#include <libavutil/avstring.h>
}

struct buffer_data {
    uint8_t *ptr;
    size_t size; ///< size left in the buffer
};

class FileManager
{
	public:
		static bool createBlankFile(std::string filename, unsigned long long fSize = 0)
		{
			std::ofstream os(filename, std::ios::out | std::ios::binary);
	
			if (os.is_open()) 
			{
				if (fSize > 0)
				{
					os.seekp(fSize - 1);
					os.write("", 1);
				}
				
				os.close();
				
				return true;
			}

			return false;
		}
		
		static char* readFromFile(std::string filename, unsigned offset, unsigned len)
		{
			char *buffer;
			std::ifstream is(filename, std::ios::ate | std::ios::binary);
			
			if (is.is_open())
			{
				buffer = new char[len];
				is.seekg(offset, std::ios::beg);
				is.read(buffer, len);
				buffer[len] = '\0';
			}

			return buffer;
		}
		
		static bool writeToFile(std::string filename, char* buffer, unsigned offset, unsigned len)
		{
			std::ofstream os(filename, std::ios::in | std::ios::out | std::ios::binary);
	
			if (os.is_open())
			{
				os.seekp(offset, std::ios::beg);
				os.write(buffer, len);
				os.close();
				
				return true;
			}

			return false;
		}

		static unsigned long long getFileSize(std::string filename)
		{
			std::ifstream is(filename, std::ios::ate | std::ios::binary);
	
			if (is.is_open())
			{
				is.seekg(0, std::ios::end);
				unsigned long long fSize = is.tellg();
				is.close();
				
				return fSize;
			}
			
			return 0;
		}
		
		static void traverseDirectory(char* dirtree, const char* path, int idx = 0)
		{
			DIR *dir = opendir(path);
			
			if (dir != NULL)
			{
				struct dirent *entry = readdir(dir);
				strcat(dirtree, "{");
				
				while (entry != NULL)
				{
					if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
					{
						char buffer[256];
						strcpy(buffer, path);
						strcat(buffer, "\\");
						strcat(buffer, entry->d_name);

						DIR *dir2 = opendir(buffer);
						
						if (idx) strcat(dirtree, ",");
						
						char str[80];
						
						if (dir2 != NULL)
						{
							sprintf(str, "\"%s\":", entry->d_name);
							strcat(dirtree, str);
							closedir(dir2);
							traverseDirectory(dirtree, buffer);
						}
						else
						{
							sprintf(str, "\"%d\": {\"name\":\"%s\",\"size\":%llu}", idx++, entry->d_name, getFileSize(buffer));
							strcat(dirtree, str);
						}
					}
					
					entry = readdir(dir);
				}
				
				strcat(dirtree, "}");
				closedir(dir);
			}
		}

		static std::string get_metadata(char* path)
		{
			AVFormatContext *m_fmt_ctx = NULL;
			AVIOContext *m_avio_ctx = NULL;
			uint8_t *m_buffer = NULL, *m_avio_ctx_buffer = NULL;
			size_t m_buffer_size, m_avio_ctx_m_buffer_size = 4096;
			struct buffer_data m_bd = { 0 };
		
			std::string data = "{";
	
			/* slurp file content into buffer */
			int ret = av_file_map(path, &m_buffer, &m_buffer_size, 0, NULL);
			
			if (ret < 0)
			{
				//cleanup();
				cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
				return "";
			}

			/* fill opaque structure used by the AVIOContext read callback */
			m_bd.ptr  = m_buffer;
			m_bd.size = m_buffer_size;

			if (!(m_fmt_ctx = avformat_alloc_context())) {
				ret = AVERROR(ENOMEM);
				cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
				return "";
			}

			m_avio_ctx_buffer = (uint8_t *)av_malloc(m_avio_ctx_m_buffer_size);
			if (!m_avio_ctx_buffer) {
				ret = AVERROR(ENOMEM);
				cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
				return "";
			}
			m_avio_ctx = avio_alloc_context(m_avio_ctx_buffer, m_avio_ctx_m_buffer_size,
										  0, &m_bd, &read_packet, NULL, NULL);
			if (!m_avio_ctx) {
				ret = AVERROR(ENOMEM);
				cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
				return "";
			}
			m_fmt_ctx->pb = m_avio_ctx;

			ret = avformat_open_input(&m_fmt_ctx, NULL, NULL, NULL);
			if (ret < 0) {
				fprintf(stderr, "Could not open input\n");
				cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
				return "";
			}

			ret = avformat_find_stream_info(m_fmt_ctx, NULL);
			if (ret < 0) {
				fprintf(stderr, "Could not find stream information\n");
				cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
				return "";
			}

			int64_t duration = m_fmt_ctx->duration + (m_fmt_ctx->duration <= INT64_MAX - 5000 ? 5000 : 0);

			data += format_data("duration", std::to_string(duration), false);
			data += format_data("streams", std::to_string(m_fmt_ctx->nb_streams), false);

			AVDictionaryEntry *tag = NULL;
					
			av_dict_get(m_fmt_ctx->metadata, "language", NULL, 0);
			tag = av_dict_get(m_fmt_ctx->metadata, "major_brand", tag, AV_DICT_IGNORE_SUFFIX);
			const char *p = tag->value;
			
			char tmp[256];
			size_t len = strcspn(p, "\x8\xa\xb\xc\xd");
			av_strlcpy(tmp, p, FFMIN(sizeof(tmp), len + 1));

			data += format_data("container", std::string(tmp), true);
			
			// codec parser
			char buf[256] = {'\0'};
			int i;
			
			for (i = 0; i < m_fmt_ctx->nb_streams; ++i)
			{
				AVStream *st = m_fmt_ctx->streams[i]; // 1 - audio
				AVCodecContext *avctx;

				avctx = avcodec_alloc_context3(NULL);
				if (!avctx)
				{
					cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
					return "";
				}

				ret = avcodec_parameters_to_context(avctx, st->codecpar);
				if (ret < 0) {
					avcodec_free_context(&avctx);
					cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
					return "";
				}

				if (avctx->codec_type == AVMEDIA_TYPE_VIDEO || avctx->codec_type == AVMEDIA_TYPE_AUDIO)
				{
					const char *codec_type = av_get_media_type_string(avctx->codec_type);
					const char *codec_name = avcodec_get_name(avctx->codec_id);
					const char *profile = avcodec_profile_name(avctx->codec_id, avctx->profile);

					if (avctx->codec_type == AVMEDIA_TYPE_VIDEO)
					{
						data += format_data("video_codec_type", codec_type, true);
						data += format_data("video_codec_name", codec_name, true);
						data += format_data("video_profile", profile, true);
					}
					else
					{
						data += format_data("audio_codec_type", codec_type, true);
						data += format_data("audio_codec_name", codec_name, true);
						data += format_data("audio_profile", profile, true);
					}

					if (avctx->codec_type == AVMEDIA_TYPE_VIDEO)
					{
						if (avctx->width) {
							data += format_data("dimension", std::to_string(avctx->width) + "x" + std::to_string(avctx->height), true);
							data += format_data("aspect_ratio", std::to_string((float)avctx->width / avctx->height), false);
						}

						data += format_data("video_resolution", av_get_pix_fmt_name(avctx->pix_fmt), true);
						data += format_data("bit_depth", std::to_string(av_pix_fmt_desc_get(avctx->pix_fmt)->comp[0].depth), false);
						
						//int fps = st->avg_frame_rate.den && st->avg_frame_rate.num;
						data += format_data("video_frame_rate", std::to_string(st->avg_frame_rate.num), false);
					}
					
					int64_t bitrate;
					bitrate = get_bit_rate(avctx);
					
					if (avctx->codec_type == AVMEDIA_TYPE_VIDEO)
					{
						data += format_data("video_bitrate", std::to_string(bitrate), false);
					}
					else if (avctx->codec_type == AVMEDIA_TYPE_AUDIO)
					{
						data += format_data("audio_bitrate", std::to_string(bitrate), false);
						
						if (avctx->sample_rate)
						{
							data += format_data("sampling_rate", std::to_string(avctx->sample_rate), false);
						}

						data += format_data("channels", std::to_string(avctx->channels), false);
						
						av_get_channel_layout_string(buf + strlen(buf), 256 - strlen(buf), avctx->channels, avctx->channel_layout);
						data += format_data("channel_layout", std::string(buf), true, true);
					}
				}
				
				avcodec_free_context(&avctx);
			}

			data += "}";

			cleanup(m_fmt_ctx, m_avio_ctx, m_buffer, m_buffer_size);
			
			return data;
		}
		
	private:
		/*static AVFormatContext *m_fmt_ctx;
		static AVIOContext *m_avio_ctx;
		static uint8_t *m_buffer, *m_avio_ctx_buffer;
		static size_t m_buffer_size;
		static size_t m_avio_ctx_m_buffer_size;

		static AVFormatContext *m_fmt_ctx = NULL;
		static AVIOContext *m_avio_ctx = NULL;
		static uint8_t *m_buffer = NULL, *m_avio_ctx_buffer = NULL;
		static size_t m_buffer_size, m_avio_ctx_m_buffer_size = 4096;
		static char *m_input_filename = NULL;
		static struct buffer_data m_bd = { 0 };*/
		
		static std::string format_data(std::string key, std::string value, bool isString, bool isEnd = false)
		{
				std::string comma = !isEnd ? "," : "";
	
				if (isString)
					return "\"" + key + "\":\"" + value + "\"" + comma;
				
				return "\"" + key + "\":" + value + comma;
		}
		
		static int read_packet(void *opaque, uint8_t *buf, int buf_size)
		{
			struct buffer_data *m_bd = (struct buffer_data *)opaque;
			buf_size = FFMIN(buf_size, m_bd->size);

			if (!buf_size)
				return AVERROR_EOF;

			/* copy internal buffer data to buf */
			memcpy(buf, m_bd->ptr, buf_size);
			m_bd->ptr += buf_size;
			m_bd->size -= buf_size;

			return buf_size;
		}
		
		static int64_t get_bit_rate(AVCodecContext *ctx)
		{
			int64_t bit_rate;
			int bits_per_sample;

			switch (ctx->codec_type) 
			{
				case AVMEDIA_TYPE_VIDEO:
				case AVMEDIA_TYPE_DATA:
				case AVMEDIA_TYPE_SUBTITLE:
				case AVMEDIA_TYPE_ATTACHMENT:
					bit_rate = ctx->bit_rate;
					break;
					
				case AVMEDIA_TYPE_AUDIO:
					bits_per_sample = av_get_bits_per_sample(ctx->codec_id);
					bit_rate = bits_per_sample ? ctx->sample_rate * (int64_t)ctx->channels * bits_per_sample : ctx->bit_rate;
					break;
					
				default:
					bit_rate = 0;
					break;
			}
			
			return bit_rate;
		}

		static void cleanup(AVFormatContext *m_fmt_ctx, AVIOContext *m_avio_ctx, uint8_t *m_buffer, size_t m_buffer_size)
		{
			avformat_close_input(&m_fmt_ctx);
	
			/* note: the internal buffer could have changed, and be != m_avio_ctx_buffer */
			if (m_avio_ctx) {
				av_freep(&m_avio_ctx->buffer);
				av_freep(&m_avio_ctx);
			}
			av_file_unmap(m_buffer, m_buffer_size);
		}

};
