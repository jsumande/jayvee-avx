#pragma once

#include "serializer/json.hpp"

#define SHARD_SIZE 2048000
#define MAX_BUFFER 65536
#define BUFFER_SIZE 32768

#define KEY_PREFIX "AVX"

// paths
#define CURRENCY_CHAIN_PATH "data/currency_chain.txt"
#define ASSET_CHAIN_PATH "data/asset_chain.txt"

typedef unsigned int UINT;
typedef unsigned long long ULL;

using json = nlohmann::json;

enum 
{
	// blockchain transaction type
	BLOCKCHAIN_TX_FUND = 1,
	BLOCKCHAIN_TX_UPLOAD,
	BLOCKCHAIN_TX_DOWNLOAD,
	
	// p2p network
	P2P_CONNECTION = 1111,
	ASSIGN_PUBKEY_IP,
	
	// account sign-up / sign-in
	REGISTRATION,
	LOGIN,
	
	// transaction process
	FUND_TRANSFER,
	FUND_TRANSFER_UPLOAD,
	FUND_TRANSFER_DOWNLOAD,

	//consensus process
	START_VALIDATION,
	CREATE_BLOCK,
	RECEIVE_BLOCK,
	
	// requests
	GET_BALANCE,
	GET_ASSETS,

	DOWNLOAD_COMPLETE = 6060,
	RECEIVE_CONSENSUS_HASHES,
	//transaction server

	// electron message
	ROUTING_TABLE = 7000
};
