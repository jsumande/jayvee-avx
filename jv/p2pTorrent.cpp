#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <thread>
#include <unordered_map>
#include <map>
#include <typeinfo>
#include <time.h>

#include "P2PNetwork.hpp"
#include "json.hpp"
#include "helpers/FileManager.hpp"

using json = nlohmann::json;

typedef unsigned int UINT;
typedef unsigned long long ULL;

// n = # of nodes
// max(floor(log(2^n)), 1)

P2PNetwork *p2p;

UINT port = 5150;
std::string electronIP = "127.0.0.1";

std::string connectionInfo[2];
std::string disconnectionInfo[2];
std::string messageInfo[2];

int main(int argc, char** argv)
{
	if (argc > 1) port = std::stoi(argv[1]);

	p2p = new P2PNetwork(port);

	if(p2p->start())
	{
		printf("P2P Network is running..\n");
		
		while(true)
		{
			if(p2p->getLocalServer()->hasActivity())
			{
				if(p2p->getLocalServer()->acceptNewConnection(connectionInfo))
				{
					printf("New Connection IP: %s, Port: %s\n", connectionInfo[0].c_str(), connectionInfo[1].c_str());
				}

				// receive message from client
				if(p2p->getLocalServer()->onMessage(messageInfo))
				{
					printf("Message from client IP: %s, Message: %s\n", messageInfo[0].c_str(), messageInfo[1].c_str());
					
					json msg = json::parse(messageInfo[1]);
					int status = msg["status"].get<int>();
					
					if (status == 1)
					{
						// do something
						std::cout << "Status 1 Received" << std::endl;
						
						json a;
						a["status"] = 3;
						a["data"] = "hello";
						p2p->sendToClient(messageInfo[0], port, &(a.dump())[0u]);
					}
					else if (status == 1112)
					{
						//std::cout << "Signup" << std::endl;
						std::cout << messageInfo[0] << std::endl;
						json a;
						a["status"] = 1112;
						a["data"] = 1;
						p2p->sendToClient(messageInfo[0], port, &(a.dump())[0u]);
					}
					else if (status == 1113)
					{
						//std::cout << "Try lang ni jv" << std::endl;
						//std::cout << msg.dump() << std::endl;
						json a;
						a["status"] = 1113;
						a["data"] = 1;
						p2p->sendToClient(messageInfo[0], port, &(a.dump())[0u]);
					}
					else if (status == 1118)
					{
						std::cout << "Wallet Balance" << std::endl;
						//std::cout << msg.dump() << std::endl;
						json a;
						a["status"] = 15;
						a["data"] = "hello";
						p2p->sendToClient(messageInfo[0], port, &(a.dump())[0u]);
					}
					
					else if (status == 30)
					{
						//std::cout << "Try lang ni jv" << std::endl;
						//std::cout << msg.dump() << std::endl;
						json a;
						a["status"] = 15;
						a["data"] = "hello";
						p2p->sendToClient(messageInfo[0], port, &(a.dump())[0u]);
					}
					else if (status == 1120) {
						const char *path = status["filenamePath"];
						char dirtree[65000] = {'\0'};
						FileManager::traverseDirectory(dirtree, path);
						std::string tree = dirtree;
						std::cout << tree;						
						
					}
						
				}
				
				//receive message from server
				p2p->recvFromServer([](char* str) {
					//std::string decoded = base64_decode(encoded);
				});

				if(p2p->getLocalServer()->hasNewDisconnection(disconnectionInfo))
				{
					printf("New Disconnection IP: %s, Port: %s\n", disconnectionInfo[0].c_str(), disconnectionInfo[1].c_str());
				}
			}
		}
	}
	else printf("P2P Network cannot run..\n");
	return 0;
}
