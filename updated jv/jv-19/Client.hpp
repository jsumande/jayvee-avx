#pragma once

#include <iostream>
#include <stdio.h>
#include <string>
#include <winsock2.h>

typedef unsigned int UINT;
typedef std::string string;

class Client
{
    private:
    UINT port;
    string ip;
    WSADATA wsaData;
    SOCKET SendingSocket;
    SOCKADDR_IN ServerAddr, ThisSenderInfo; //Server/receiver address
    int RetCode;
    // Be careful with the array bound, provide some checking mechanism...
    int BytesSent, nlen;
    bool running;
	char buffer[65000 + 512] = { 0 };
	
	//set of socket descriptors 
	fd_set readfds;
    struct timeval timeout;

    public:
    Client(UINT, string);
    void sendMessage(char[]);
    bool init();
    bool start();
    void stop();
    bool run();
    bool isRunning();
	bool onMessage(std::string *info);
	std::string getIP();
	UINT getPort();
	
	bool isArtifact;
	float inactivityTimer;
};

Client::Client(UINT port, string ip)
{
    this->port = port;
    this->ip = ip;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	this->isArtifact = false;
	this->inactivityTimer = 0.0f;
}

bool Client::run()
{
    if(this->init())
    {
        this->start();
        return true;
    }

    return false;
}

bool Client::init()
{
    WSAStartup(MAKEWORD(2,2), &wsaData);

    // Create a new socket to make a client connection.
    // AF_INET = 2, The Internet Protocol version 4 (IPv4) address family, TCP protocol
     SendingSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

     if(SendingSocket == INVALID_SOCKET)
     {
          printf("Client: socket() failed! Error code: %ld\n", WSAGetLastError());
          // Do the clean up
          WSACleanup();
          // Exit with error
          return false;
     }

    // Set up a SOCKADDR_IN structure that will be used to connect
    // to a listening server on port 5150. For demonstration
    // purposes, let's assume our server's IP address is 127.0.0.1 or localhost

    // IPv4
    ServerAddr.sin_family = AF_INET;
    // Port no.
    ServerAddr.sin_port = htons(this->port);
    // The IP address
    //char *ip = "127.0.0.1";
    ServerAddr.sin_addr.s_addr = inet_addr(this->ip.c_str());
    //ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    return true;
}

bool Client::start()
{
    // Make a connection to the server with socket SendingSocket.
    RetCode = connect(SendingSocket, (SOCKADDR *) &ServerAddr, sizeof(ServerAddr));

    if (RetCode != 0)
    {
        printf("Client: connect() failed! Error code: %ld\n", WSAGetLastError());
        // Close the socket
        closesocket(SendingSocket);
        // Do the clean up
        WSACleanup();
        // Exit with error
        return false;
    }
    // else
    // {
    //     printf("Client: connect() is OK, got connected...\n");
    //     printf("Client: Ready for sending and/or receiving data...\n");
    // }

    // At this point you can start sending or receiving data on
    // the socket SendingSocket.
    // Some info on the receiver side...

    getsockname(SendingSocket, (SOCKADDR *)&ServerAddr, (int *)sizeof(ServerAddr));
    //printf("Client: Receiver IP(s) used: %s\n", inet_ntoa(ServerAddr.sin_addr));
    //printf("Client: Receiver port used: %d\n", htons(ServerAddr.sin_port));

	const int bufferSize = 65512;
    setsockopt(SendingSocket, SOL_SOCKET, SO_SNDBUF, (char*)&bufferSize, sizeof(bufferSize));
    setsockopt(SendingSocket, SOL_SOCKET, SO_RCVBUF, (char*)&bufferSize, sizeof(bufferSize));
	
    this->running = true;
    return true;
}

void Client::stop()
{
    printf("Client Stopped\n");
    // Below code is to close the client

    if (shutdown(SendingSocket, SD_SEND) != 0)
        printf("Client: Well, there is something wrong with the shutdown(). The error code: %ld\n", WSAGetLastError());
    else 
        printf("Client: shutdown() looks OK...\n");

    // When you are finished sending and receiving data on socket SendingSocket,
    // you should close the socket using the closesocket API. We will
    // describe socket closure later in the chapter.

    if(closesocket(SendingSocket) != 0)
        printf("Client: Cannot close \"SendingSocket\" socket. Error code: %ld\n", WSAGetLastError());
    else printf("Client: Closing \"SendingSocket\" socket...\n");

    // When your application is finished handling the connection, call WSACleanup.
    if(WSACleanup() != 0) printf("Client: WSACleanup() failed!...\n");
    else printf("Client: WSACleanup() is OK...\n");

    this->running = false;
}

void Client::sendMessage(char sendBuff[])
{
    BytesSent = send(SendingSocket, sendBuff, strlen(sendBuff), 0);

    if (BytesSent == SOCKET_ERROR) 
    {
        printf("Client: send() error %ld.\n", WSAGetLastError());
    }
    else 
    {
        // printf("Client: send() is OK - bytes sent: %ld\n", BytesSent);
        // Some info on this sender side...
        // Allocate the required resources
        memset(&ThisSenderInfo, 0, sizeof(ThisSenderInfo));
        nlen = sizeof(ThisSenderInfo);
        getsockname(SendingSocket, (SOCKADDR *)&ThisSenderInfo, &nlen);
        
        // printf("Client: Sender IP(s) used: %s\n", inet_ntoa(ThisSenderInfo.sin_addr));
        // printf("Client: Sender port used: %d\n", htons(ThisSenderInfo.sin_port));
        // printf("Client: Those bytes represent: \"%s\"\n", sendBuff);
    }
}

bool Client::isRunning()
{
    return this->running;
}

bool Client::onMessage(std::string *info)
{
	//clear the socket set 
    FD_ZERO(&readfds);  

    //add master socket to set 
    FD_SET(SendingSocket, &readfds);
	
	
	int activity = select(SendingSocket + 1, &readfds , NULL , NULL , &timeout);
	
	if (activity > 0)
	{
		this->inactivityTimer = 0;
		
		if (FD_ISSET(SendingSocket, &readfds))
		{
			int valread = recv(SendingSocket , buffer, 65000, 0);
			//std::cout << "valread " << valread << std::endl;
			
			if (valread == 0 || valread == SOCKET_ERROR)  //if someone disconnected
			{  
				/*//valread = 0            -> intentional close
				//valread = SOCKET_ERROR -> unintentional close

				//Somebody disconnected , get his details and print 
				getpeername(sd , (struct sockaddr*)&clients[sd]->ip, (int *)&addrlen);  
				// printf("Host disconnected , ip %s , port %d \n" , s
				//         inet_ntoa(serverAddr.sin_addr) , ntohs(serverAddr.sin_port));  

				//Close the socket and mark as 0 in list for reuse 
				newDisconnectedClient = new ClientInfo(clients[sd]->ip, std::to_string(this->port));

				close(sd);  
				clients.erase(sd);*/

				return false;
			}  
			else //Echo back the message that came in 
			{  
				//set the string terminating NULL byte on the end of the data read
				buffer[valread] = '\0';
				
				info[0] = this->ip.c_str(); //IP
				info[1] = buffer; //Message
				
				std::cout << info[1];
				
				memset(buffer, 0, sizeof(buffer)); //Clearing Buffer
				return true;
			}
		}
	}

    return false;
}

std::string Client::getIP()
{
    return this->ip;
}

UINT Client::getPort()
{
    return this->port;
}

