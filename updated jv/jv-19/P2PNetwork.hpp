#pragma once

#include <stdio.h>
#include <string>
#include <winsock2.h>
#include "Client.hpp"
#include "Server.hpp"
#include <map>
#include <vector>
#include <functional>
#define INACTIVITY_LIMIT 5

typedef unsigned int UINT;

class P2PNetwork
{
    private:
    UINT port;
    Server *localServer;
    std::map<std::string, Client*> userServer;
    
    public:
    P2PNetwork(UINT);
    void sendMessage(std::string, char []);
    bool start();
    void disconnect(std::string);
    bool connect(std::string);
    bool connect(UINT, std::string);
    Server* getLocalServer();
	bool isConnected(std::string ip);
	
	void requestToServer(char []);
	void recvFromServer(std::function<void(char*)> callback);
	void findArtifactConnections(std::map<std::string, int> connections);
	void deleteInactiveArtifacts(float dt);
	void sendToClient(std::string ip, char sendBuff[]);
	std::string getPrivateIP();
};

P2PNetwork::P2PNetwork(UINT port)
{
    this->port = port;
    this->localServer = new Server(port);
}

Server* P2PNetwork::getLocalServer()
{
    return this->localServer;
}

void P2PNetwork::findArtifactConnections(std::map<std::string, int> connections)
{
	if (connections.size() > 0) {
		for (auto &n: this->userServer) {
			if (connections.count(n.second->getIP()) == 0)
			{
				std::cout << "Artifact connection: " << n.second->getIP() << std::endl;
				n.second->isArtifact = true;
			} else {
				std::cout << "Non-Artifact connection: " << n.second->getIP() << std::endl;
			}
			
			//n.second->isArtifact = true;
		}
	}
}

void P2PNetwork::deleteInactiveArtifacts(float dt)
{
	for (auto &n: this->userServer) {
		if (n.second->isArtifact)
		{
			n.second->inactivityTimer += dt;
			if ((int)n.second->inactivityTimer == INACTIVITY_LIMIT && n.second->isRunning()) {
				n.second->stop();
				this->userServer.erase(n.first);
			}
		}
	}
}

bool P2PNetwork::start()
{
    if(localServer->init())
	{
		if(localServer->createMasterSocket())
		{
            return true;
        }
    }

    return false;
}

void P2PNetwork::disconnect(std::string ip)
{
	if (this->userServer[ip]->isRunning()) 
	{
		this->userServer[ip]->stop();
	}
}

bool P2PNetwork::connect(std::string ip)
{
    if(this->connect(this->port, ip)) return true;
    return false;
}

bool P2PNetwork::connect(UINT port, std::string ip)
{
	if (this->userServer.count(ip) == 0) // connect if ip is not connected
	{
		Client *newUserServer = new Client(port, ip);
		if(newUserServer->init())
		{
			if(newUserServer->start())
			{
				this->userServer[ip] = newUserServer;
				return true;
			}
		}
	}
    
    return false;
}

bool P2PNetwork::isConnected(std::string ip)
{
	return (this->userServer.count(ip) > 0); 
}

// send as client
void P2PNetwork::sendMessage(std::string ip, char sendBuff[])
{
    this->userServer[ip]->sendMessage(sendBuff);
}

// send as client
void P2PNetwork::sendToClient(std::string ip, char sendBuff[])
{
    this->localServer->sendToClient(ip, sendBuff);
}

void P2PNetwork::requestToServer(char sendBuff[])
{
	for (auto const& s: this->userServer)
	{
		s.second->sendMessage(sendBuff);
	}
}

void P2PNetwork::recvFromServer(std::function<void(char*)> callback)
{
	std::string msg[2];
	
	//std::cout << "Message from server!!!" << std::endl;
	
	for (auto const& s: this->userServer)
	{
		if (s.second->onMessage(msg))
		{
			//printf("Message from server IP: %s, Message: %s\n", msg[0].c_str(), msg[1].c_str());
			callback((char*)msg[1].c_str());
		}
	}
}

std::string P2PNetwork::getPrivateIP()
{
    char ac[80];
    std::string privateIP = "";

    if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR)
        return "";

    struct hostent *phe = gethostbyname(ac);

    if (phe == 0)
        return "";

    for (int i = 0; phe->h_addr_list[i] != 0; ++i)
    {
        struct in_addr addr;
        memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
        privateIP = inet_ntoa(addr);
    }

    return privateIP;
}


