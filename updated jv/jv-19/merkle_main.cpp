#include <iostream>
#include <vector>

#include "picosha2.h"

std::string merkle(std::vector<std::string> hashes);
std::string merkleHash(std::string h1, std::string h2);
std::string reverseStr(std::string str);

int main()
{
	std::vector<std::string> H;
	std::string line;
	
	while(std::cin >> line)
	{
		//std::cout << line << std::endl;
		H.push_back(line);
	}
	
	//std::cout << merkle(H) << std::endl;
	std::cout << merkleHash(H[0], H[1]) << std::endl;
	
	return 0;
}

std::string merkle(std::vector<std::string> hashes)
{
	std::vector<std::string> newHashList;
	int len = hashes.size() ;
	int maxIdx = len - 1;
	
	if (len == 1) return hashes[0];
	
	for (int i = 0; i < maxIdx; i += 2)
	{
		//std::cout << i + 1 << std::endl;
		newHashList.push_back(merkleHash(hashes[i], hashes[i + 1]));
	}
	
	if (len & 1) 
		newHashList.push_back(merkleHash(hashes[maxIdx], hashes[maxIdx]));
	
	return merkle(newHashList);
}

std::string merkleHash(std::string h1, std::string h2)
{
	return picosha2::hash256_hex_string(h1 + h2);
}

//871714dcbae6c8193a2bb9b2a69fe1c0440399f38d94b3a0f1b447275a29978a
