#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <thread>
#include <unordered_map>
#include <map>
#include <typeinfo>
#include <time.h>

#include "P2PNetwork.hpp"
#include "json.hpp"

using json = nlohmann::json;

typedef unsigned int UINT;
typedef unsigned long long ULL;

// n = # of nodes
// max(floor(log(2^n)), 1)

P2PNetwork *p2p;

UINT port = 5150;
std::string electronIP = "192.168.254.109";
UINT electronPort = 7070;

std::string connectionInfo[2];
std::string disconnectionInfo[2];
std::string messageInfo[2];

std::string privateIP;

int main(int argc, char** argv)
{
	if (argc > 1) port = std::stoi(argv[1]);

	p2p = new P2PNetwork(port);

	if(p2p->start())
	{
		std::cout << "P2P Network is running..\n";
		
		privateIP = p2p->getPrivateIP();
		std::cout << privateIP << std::endl;
		
		if(p2p->connect(electronPort, electronIP)) 
		{
			std::cout << "Connected to Port: " << electronPort;
			std::cout << " IP: " << electronIP << std::endl;
			
			json j;
			j["status"] = 1;
			j["privateIP"] = privateIP;
			
			p2p->sendMessage(electronIP, &(j.dump())[0u]);
		}
		else
		{
			std::cout << "Cannot connect to Port: " << electronPort;
			std::cout << " IP: " << electronIP << std::endl;
		}
		
		while(true)
		{
			if(p2p->getLocalServer()->hasActivity())
			{
				std::cout << "vee" << std::endl;
				
				if(p2p->getLocalServer()->acceptNewConnection(connectionInfo))
				{
					std::cout << "New Connection IP: " << connectionInfo[0];
					std::cout << ", Port: " << connectionInfo[1] << std::endl;
				}

				// receive message from client
				if(p2p->getLocalServer()->onMessage(messageInfo))
				{
					std::cout << "Message from client IP: " << messageInfo[0];
					std::cout << ", Message: " << messageInfo[1] << std::endl;
					
					json msg = json::parse(messageInfo[1]);
					int status = msg["status"].get<int>();
					
					if (status == 1)
					{
						// do something
						std::cout << msg.dump() << std::endl;
					}
					else if (status == 2)
					{
						if(p2p->connect(port, messageInfo[0])) 
						{
							std::cout << "Connected to Port: " << port;
							std::cout << " IP: " << messageInfo[0] << std::endl;
						}
						else
						{
							std::cout << "Cannot connect to Port: " << port;
							std::cout << " IP: " << messageInfo[0] << std::endl;
						}
					}
				}

				if(p2p->getLocalServer()->hasNewDisconnection(disconnectionInfo))
				{
					std::cout << "New Disconnection IP: " << disconnectionInfo[0];
					std::cout << ", Port: " << disconnectionInfo[1] << std::endl;
				}
			}
			
			//receive message from server
			p2p->recvFromServer([](char* str) {
				json msg = json::parse(std::string(str));
				int status = msg["status"].get<int>();
				
				if (status == 1)
				{
					//std::cout << msg["rt"][0]["public_ip"].get<std::string>() << std::endl;
					for (auto const& m: msg["rt"])
					{
						std::cout << m["ip"].get<std::string>() << std::endl;
						if (m["type"].get<int>() == 1)
						{
							std::string ip = m["ip"].get<std::string>();
							std::cout << "Connecting to: " << ip << " " << privateIP << std::endl;
							if (privateIP != ip)
							{
								if(p2p->connect(port, ip)) 
								{
									std::cout << "Connected to Port: " << port;
									std::cout << " IP: " << ip << std::endl;
									
									json j;
									j["status"] = 2;
									
									p2p->sendMessage(ip, &(j.dump())[0u]);
								}
								else
								{
									std::cout << "Cannot connect to Port: " << port;
									std::cout << " IP: " << ip << std::endl;
								}
							}
						}
						else
						{
							// public
						}
					}
				}
			});
		}
	}
	else printf("P2P Network cannot run..\n");
	return 0;
}
